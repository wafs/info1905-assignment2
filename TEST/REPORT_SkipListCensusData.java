import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class REPORT_SkipListCensusData {

	private SkipList<String, Double> skipList;

	public REPORT_SkipListCensusData() {
		 skipList = new SkipList<String, Double>();
	}

	public void exploreData(String pathToFile, int i)
			throws FileNotFoundException, IOException {

		BufferedReader br = new BufferedReader(new FileReader(pathToFile));
		try {
			String line = br.readLine();
			while (line != null) {
				String[] pieces = line.trim().split("\\s+");
				if (pieces.length == 4) {
					skipList.put(pieces[0],
							Double.parseDouble(pieces[1]));
				}
				line = br.readLine();
			}
		} finally {
			br.close();
		}
		
		System.out.println(skipList.searchSteps("JAMES"));
		System.out.println(skipList.searchSteps("THOMAS"));
		System.out.println(skipList.searchSteps("RUPERT"));
		System.out.println(skipList.searchSteps("JIMMY"));
		System.out.println(skipList.searchSteps("BENEDICT"));
		System.out.println(skipList.searchSteps("LYNWOOD"));
		System.out.println(skipList.searchSteps("TOBY"));
		
		
		
	}

	public static void main(String[] args) {
		REPORT_SkipListCensusData rc = new REPORT_SkipListCensusData();

		String s = "/Users/wafs/Documents/workspace_1903/Assignment 2/dist.male.first";

			try {
				rc.exploreData(s, 0);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		// search for randomly chosen names
		System.out.println();
	}

}
