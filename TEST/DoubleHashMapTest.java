import static org.junit.Assert.*;

import org.junit.Test;

public class DoubleHashMapTest {
	DoubleHashMap<String, Double> dhm;

	@Test
	public void testDefault() {
		dhm = new DoubleHashMap<String, Double>(10, 13, 27);
		assertEquals(4000, dhm.getNodeArray().length);
	}

	@Test
	public void testLargeArraySize() {
		dhm = new DoubleHashMap<String, Double>(10002, 10, 13, 27);
		assertEquals(10002, dhm.getNodeArray().length);
		assertNull(dhm.getNodeArray()[9742]);
		assertNull(dhm.getNodeArray()[11]);
	}

	@Test
	public void testCollisionCounter() {
		dhm = new DoubleHashMap<String, Double>(30, 10, 13, 17);

		dhm.put("Hollow", 0.5);
		dhm.put("Shit", 1.0);
		dhm.put("Cherryberrycra", 1.5);
		dhm.put("Cartoon", 2.0);
		dhm.put("Heaven", 2.2);
		dhm.put("Hell", 2.2);
		dhm.put("The garden of eden", 2.2);
		dhm.put("Succubus", 2.2);
		dhm.put("Satan", 2.2);
		dhm.put("Silent beast", 14.14);
		dhm.put("Mark of the beast", 14.14);
		dhm.put("COWABUNGA", 14.14);
		dhm.put("The Legend of Zelda", 14.14);
		dhm.put("Ocarina of Time", 14.14);
		dhm.put("Garbage b0gs", 14.14);
		dhm.put("fortune cookie", 14.14);
		dhm.put("The chronicles of narnia", 14.14);
		dhm.put("Well that was awkward~", 14.14);
		dhm.put("Chim chimminy", 14.14);
		dhm.put("Cheree", 14.14);
		dhm.put("Apple Martini", 14.14);
		dhm.put("Jafar", 14.14);
		dhm.put("The Elder Scrolls : Skyrim", 14.14);
		dhm.put("Lightning Returns : Final Fantasy XIII", 14.14);

		// Collision counter
		assertEquals(13, dhm.putCollisions());
		assertEquals(17, dhm.totalCollisions());
		assertEquals(4, dhm.maxCollisions());

		// Reset to zero
		dhm.resetStatistics();
		assertEquals(0, dhm.putCollisions());
		assertEquals(0, dhm.totalCollisions());
		assertEquals(0, dhm.maxCollisions());

	}

	@Test
	public void testZeroCollisionCounters() {
		dhm = new DoubleHashMap<String, Double>(30, 10, 13, 17);

		assertEquals(0, dhm.putCollisions());
		assertEquals(0, dhm.totalCollisions());
		assertEquals(0, dhm.maxCollisions());

		dhm.put("Cheree", 0.0);

		assertEquals(0, dhm.putCollisions());
		assertEquals(0, dhm.totalCollisions());
		assertEquals(0, dhm.maxCollisions());

	}

	@Test
	public void testGet() {
		dhm = new DoubleHashMap<String, Double>(30, 10, 13, 17);
		dhm.put("Mario", 2.0);
		dhm.put("Peach", 2.5);
		dhm.put("Yoshi", 3.0);

		assertEquals(2.0, (double) dhm.get("Mario"), 0.01);

	}
	
	@Test
	public void testDefunct(){
		dhm = new DoubleHashMap<String, Double>(30, 10, 13, 17);
		System.out.println(dhm.hash("-1"));
		System.out.println(dhm.hash("5"));
		
		System.out.println("Bat".compareTo("Tree") > Double.NEGATIVE_INFINITY);
		
		
		dhm.put("-1", -1.0);
		dhm.put("5", 5.0);
		dhm.remove("5");
		assertEquals(dhm.get("5"), null);
		
	}
	
	@Test
	public void testRemove(){
		dhm = new DoubleHashMap<String, Double>(30, 10, 13, 17);
		dhm.put("boop", null);
		dhm.remove("boop");
		System.out.println();
	}
	
	@Test
	public void testOverwrite(){
		dhm = new DoubleHashMap<String, Double>(30, 10, 13, 17);
		dhm.put("boop", 2.0);
		dhm.put("boop", 7.1);
		System.out.println();
		
		assertEquals(7.1, (double)dhm.get("boop"), 0.001);
	}
}
