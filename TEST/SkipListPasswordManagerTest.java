import static org.junit.Assert.*;

import java.awt.List;
import java.util.ArrayList;

import org.junit.Test;

public class SkipListPasswordManagerTest {
	SkipListPasswordManager p;

	@Test
	public void testConstruction() {
		p = new SkipListPasswordManager();
		assertEquals(new ArrayList<String>(), p.listUsers());
	}

	@Test
	public void testAddUser() {
		System.out.println("--------- ADD USER ------------");

		p = new SkipListPasswordManager();
		p.addNewUser("Snow", "Villiers");

		// User already exists
		p.addNewUser("Snow", "Serah");

		assertEquals(1, p.listUsers().size());
	}

	@Test
	public void deleteUser() {

		System.out.println("--------- DELETE USER ------------");

		p = new SkipListPasswordManager();
		p.addNewUser("Promathia", "chains");

		assertEquals(1, p.listUsers().size());

		// No such user (Typo in username)
		p.deleteUser("Permafvia", "chonz");

		// Failed to authenticate user (Typo in password)
		p.deleteUser("Promathia", "hains");

		assertEquals(1, p.listUsers().size());

		p.deleteUser("Promathia", "chains");
		assertEquals(0, p.listUsers().size());
	}

	@Test
	public void testAuthenticate() {
		System.out.println("--------- TEST AUTHENTICATE ------------");

		p = new SkipListPasswordManager();
		p.addNewUser("Fang", "bahamut");
		p.addNewUser("Vanille", "Hecatoncheir");
		p.addNewUser("Hope", null);

		p.newAppPassword("Vanille", "Hecatoncheir", "Nirvana", "Weapon");
		p.newAppPassword("Vanille", "Hecatoncheir", null, "Role");

		assertTrue(p.authenticate("Fang", "bahamut"));

		assertFalse(p.authenticate("Fang", "Kain's Lance"));
		assertFalse(p.authenticate("Hope", "Ixion"));

		assertTrue(p.authenticate("Vanille", "Nirvana", "Weapon"));

		// No password found
		assertFalse(p.authenticate("Vanille", "Fang", "Weapon"));

		// No such user exists
		assertFalse(p.authenticate("Lightning", "Odin", "Eidolon"));
	}

	@Test
	public void testResetPassword() {
		System.out.println("--------- TEST RESET ------------");

		p = new SkipListPasswordManager();

		p.addNewUser("Sazh", "Ifrit");
		p.newAppPassword("Sazh", "Ifrit", "Total Eclipse", "Weapon");

		// No such user
		p.resetPassword("Barthandelus", "XIII", "OXXX");

		// Failed to Authenticate user.
		p.resetPassword("Sazh", "Turtle Eclipse", "Turtle Eclipse", "Weapon");

		assertFalse(p.authenticate("Sazh", "Turtle Eclipse", "Weapon"));
		assertTrue(p.authenticate("Sazh", "Total Eclipse", "Weapon"));
	}

	@Test
	public void testNewAppPassword() {
		System.out.println("--------- NEW APP PASSWORD ------------");
		p = new SkipListPasswordManager();

		p.addNewUser("Snow", "Shiva");
		p.newAppPassword("Snow", "Shiva", "Power Circle", "Weapon");

		// No such user exists
		p.newAppPassword("L'Cie", "Ruin", "health", "Ceith");

		// Password already set up
		p.newAppPassword("Snow", "Shiva", "Umbra", "Weapon");

		// Failed to authenticate user
		p.newAppPassword("Snow", "Shiva", "Power Circle", "Fiance");
	}

	@Test
	public void testSearchSteps(){
		p = new SkipListPasswordManager();

		p.addNewUser("Snow", "1");
		p.addNewUser("Lightning", "2");
		p.addNewUser("Serah", "3");
		p.addNewUser("Fang", "4");
		p.addNewUser("Vanille", "5");
		p.addNewUser("Sazh", "6");
		p.addNewUser("Hope", "7");
		
		
		
//		assertEquals(1, p.searchSteps("Fang"));
//		assertEquals(2, p.searchSteps("Lightning"));
//		assertEquals(3, p.searchSteps("Snow"));
//		assertEquals(4, p.searchSteps("Vanille"));
//		
		
	}

}
