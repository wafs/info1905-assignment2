import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class REPORT_CensusData {

	DoubleHashMap<String, Double> dhm;
	List<DoubleHashMap<String, Double>> dhmArray;

	public REPORT_CensusData() {
		dhmArray = new ArrayList<DoubleHashMap<String, Double>>();

		// 2000
		dhmArray.add(new DoubleHashMap<String, Double>(2000, 1, 4271, 1));
		dhmArray.add(new DoubleHashMap<String, Double>(2000, 1, 4271, 223));
		dhmArray.add(new DoubleHashMap<String, Double>(2000, 1, 4271, 647));
		// 4000
		dhmArray.add(new DoubleHashMap<String, Double>(4000, 1, 4271, 1));
		dhmArray.add(new DoubleHashMap<String, Double>(4000, 1, 4271, 223));
		dhmArray.add(new DoubleHashMap<String, Double>(4000, 1, 4271, 647));
	}

	public void exploreData(String pathToFile, int i)
			throws FileNotFoundException, IOException {

		BufferedReader br = new BufferedReader(new FileReader(pathToFile));
		try {
			String line = br.readLine();
			while (line != null) {
				String[] pieces = line.trim().split("\\s+");
				if (pieces.length == 4) {
					dhmArray.get(i).put(pieces[0],
							Double.parseDouble(pieces[1]));
				}
				line = br.readLine();
			}
		} finally {
			br.close();
		}
		// print collision statistics
		System.out.println("------");
		System.out.print(dhmArray.get(i).putCollisions());
		System.out.print(" " + dhmArray.get(i).totalCollisions());
		System.out.println(" " + dhmArray.get(i).maxCollisions());
		System.out.println();
	}

	public static void main(String[] args) {
		REPORT_CensusData rc = new REPORT_CensusData();

		String s = "/Users/wafs/Documents/workspace_1903/Assignment 2/dist.male.first";

		for (int i = 0; i < rc.dhmArray.size(); i++) {
			try {
				rc.exploreData(s, i);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
