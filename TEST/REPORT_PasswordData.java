import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class REPORT_PasswordData {
	public REPORT_PasswordData() {

	}

	public void printHashCollisions(String pathToFile)
			throws FileNotFoundException, IOException {
		DoubleHashMap<Long, List<String>> a = new DoubleHashMap<Long, List<String>>(
				100000, 1, 50000, 56897);
		PasswordManager s = new PasswordManager();
		BufferedReader br = new BufferedReader(new FileReader(pathToFile));
		try {
			String line = br.readLine();
			while (line != null) {
				String password = line.trim();
				Long passwordHash = s.hash(password);
				
				if (a.get(passwordHash) == null) {
					List<String> b = new ArrayList<String>();
					b.add(password);
					a.put(passwordHash, b);
				} else {
					a.get(passwordHash).add(password);
				}
				// if passwordHash in a, add password to its list value
				// else, instantiate a new ArrayList and add password to it
				line = br.readLine();
			}
		} finally {
			br.close();
		}
		List<Long> hashes = a.keys();
		
		for (Long hash : hashes) {
			List<String> passwords = a.get(hash);
			if (passwords.size() > 1) {
				System.out.println(passwords);
			}
		}
	}

	public static void main(String[] args) {
		REPORT_PasswordData pd = new REPORT_PasswordData();
		String s = "/Users/wafs/Documents/workspace_1903/Assignment 2/10k-common-passwords.txt";

		try {
			pd.printHashCollisions(s);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}