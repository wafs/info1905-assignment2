import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;


public class SkipListTest {

	SkipList<String, Integer> skip;
	
	@Test
	public void construction() {
		skip = new SkipList<String, Integer>();
		assertEquals(0, skip.size());
	}
	
	@Test
	public void testSize(){
		skip = new SkipList<String, Integer>();
		assertEquals(0, skip.size());
		
		skip.put("Potion", 14);
		skip.put("Ether", 5);
		assertEquals(2, skip.size());
		
		skip.remove("Potion");
		skip.remove("Ether");
		assertEquals(0, skip.size());
		
		skip.remove("Potion");
		assertEquals(0, skip.size());		
	}
	
	@Test 
	public void testCorrectNode(){
		SkipListNode n = new SkipListNode("Icarus Wing", 12);
		skip = new SkipList<String, Integer>();
		skip.put("Icarus Wing", 12);

		assertEquals(n.getValue(), skip.get("Icarus Wing"));
		assertNull(skip.get("Kraken Club"));
	}
	
	@Test
	public void testKeys(){
		skip = new SkipList<String, Integer>();
		
		skip.put("Phoenix Down", 3);
		skip.put("Eye Drops", 74);
		skip.put("Echo Drops", 12);
		skip.put("Silent Oil", 6);
		
		List<String> l = new ArrayList<String>();
		l.add("Echo Drops");
		l.add("Eye Drops");
		l.add("Phoenix Down");
		l.add("Silent Oil");
		
		
		assertEquals(l, skip.keys());
		assertFalse(skip.isEmpty());
		
		skip.put("Reraise Gorget", 1);
		skip.put("Bomb", 2);
		skip.put("Behemoth Horn", 123);

		skip.consoleSkipList();
	}
	
	@Test
	public void testPutOverwrite(){

		skip = new SkipList<String, Integer>();
		
		skip.put("Phoenix Down", 3);
		skip.put("Eye Drops", 74);
		
		assertEquals(3, (int)skip.get("Phoenix Down"));
		
		assertEquals(3,(int) skip.put("Phoenix Down", 7));
		
		assertEquals(7, (int)skip.get("Phoenix Down"));

		assertEquals(74, (int)skip.put("Eye Drops", 12));
		assertEquals(12, (int)skip.put("Eye Drops", 8));

	}

}
