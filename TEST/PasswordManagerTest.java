import static org.junit.Assert.*;

import java.awt.List;
import java.util.ArrayList;

import org.junit.Test;

public class PasswordManagerTest {
	PasswordManager p;

	@Test
	public void testConstruction() {
		p = new PasswordManager();
		assertEquals(new ArrayList<String>(), p.listUsers());
	}

	@Test
	public void testAddUser() {
		System.out.println("--------- ADD USER ------------");

		p = new PasswordManager();
		p.addNewUser("Snow", "Villiers");

		// User already exists
		p.addNewUser("Snow", "Serah");

		assertEquals(1, p.listUsers().size());
	}

	@Test
	public void deleteUser() {

		System.out.println("--------- DELETE USER ------------");

		p = new PasswordManager();
		p.addNewUser("Promathia", "chains");

		assertEquals(1, p.listUsers().size());

		// No such user (Typo in username)
		p.deleteUser("Permafvia", "chonz");

		// Failed to authenticate user (Typo in password)
		p.deleteUser("Promathia", "hains");

		assertEquals(1, p.listUsers().size());

		p.deleteUser("Promathia", "chains");
		assertEquals(0, p.listUsers().size());
	}

	@Test
	public void testAuthenticate() {
		System.out.println("--------- TEST AUTHENTICATE ------------");

		p = new PasswordManager();
		p.addNewUser("Fang", "bahamut");
		p.addNewUser("Vanille", "Hecatoncheir");
		p.addNewUser("Hope", null);

		p.newAppPassword("Vanille", "Hecatoncheir", "Nirvana", "Weapon");
		p.newAppPassword("Vanille", "Hecatoncheir", "ORK", "Weapon");
		p.newAppPassword("Vanille", "Hecatoncheir", null, "Role");
		p.newAppPassword(null, "Hecatoncheir", null, "Role");

		assertTrue(p.authenticate("Fang", "bahamut"));

		assertFalse(p.authenticate("Fang", "Kain's Lance"));
		assertFalse(p.authenticate("Hope", "Ixion"));

		assertTrue(p.authenticate("Vanille", "Nirvana", "Weapon"));

		// No password found
		assertFalse(p.authenticate("Vanille", "Fang", "Weapon"));

		// No such user exists
		assertFalse(p.authenticate("Lightning", "Odin", "Eidolon"));
	}

	@Test
	public void testResetPassword() {
		System.out.println("--------- TEST RESET ------------");

		p = new PasswordManager();

		p.addNewUser("Sazh", "Ifrit");
		p.newAppPassword("Sazh", "Ifrit", "Total Eclipse", "Weapon");

		// No such user
		p.resetPassword("Barthandelus", "XIII", "OXXX");

		// Failed to Authenticate user.
		p.resetPassword("Sazh", "Turtle Eclipse", "Turtle Eclipse", "Weapon");

		assertFalse(p.authenticate("Sazh", "Turtle Eclipse", "Weapon"));
		assertTrue(p.authenticate("Sazh", "Total Eclipse", "Weapon"));
	}

	@Test
	public void testNewAppPassword() {
		System.out.println("--------- NEW APP PASSWORD ------------");
		p = new PasswordManager();

		p.addNewUser("Snow", "Shiva");
		p.newAppPassword("Snow", "Shiva", "Power Circle", "Weapon");

		// No such user exists
		p.newAppPassword("L'Cie", "Ruin", "health", "Ceith");

		// Password already set up
		p.newAppPassword("Snow", "Shiva", "Umbra", "Weapon");

		// Failed to authenticate user
		p.newAppPassword("Snow", "Shiva", "Power Circle", "Fiance");
	}
	
	@Test
	public void testMultipleApps(){
		p = new PasswordManager();
		
		p.addNewUser("Snow", "Shiva");
		p.newAppPassword("Snow", "Shiva", "ETHER", "Magic");
		p.newAppPassword("Snow", "Shiva", "POTION", "Health");
		p.newAppPassword("Snow", "Shiva", "FISTS", "Weapon");
		
		
		assertTrue(p.authenticate("Snow", "ETHER", "Magic"));
		assertTrue(p.authenticate("Snow", "POTION", "Health"));
		assertTrue(p.authenticate("Snow", "FISTS", "Weapon"));
		
		p.newAppPassword("Snow", "Shiva", "SWORD", "Weapon");
		assertTrue(p.authenticate("Snow", "FISTS", "Weapon"));

		p.resetPassword("Snow", "FISTS", "SWORD", "Weapon");
		p.resetPassword("Snow", "FISTS", "SWORD", "ZFSJOI");
		System.out.println("FFFF");
		p.resetPassword("SNURR", "ShivJSJa", "FISTS", "JEFJ");

		
		assertTrue(p.authenticate("Snow", "SWORD", "Weapon"));


		

	}

}
