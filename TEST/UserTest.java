import static org.junit.Assert.*;

import org.junit.Test;

public class UserTest {

	User u;

	@Test
	public void testConstruction() {
		u = new User("Lightning");
		assertEquals("Lightning", u.getUsername());
	}

	@Test
	public void testAppName() {
		u = new User("Snow");
		assertNull(u.getPassword("Apple"));

	}

	@Test
	public void setPassword() {
		u = new User("Vanille");
		u.setPassword("FFXIII", 01L);

		assertEquals(01L, u.getPassword("FFXIII").longValue());

		u.setPassword("FFXIII", 777L);
		assertEquals(777L, u.getPassword("FFXIII").longValue());

	}
}
