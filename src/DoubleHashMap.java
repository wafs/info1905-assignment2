import java.util.ArrayList;
import java.util.List;

public class DoubleHashMap<K, V> {
	// construction

	// Constructor Variables
	private int hashMapSize;
	private int multiplier;
	private int modulus;
	private int secondaryModulus;
	private HashMapNode[] nodeArray;
	
	// Counters
	private int nodeCount;
	private int putCollisions;
	private int totalCollisions;
	private int maxCollisions;
	
	// Utility
	private HashMapNode defunct;
	
	
	// construct a DoubleHashMap with 4000 places and given hash parameters
	public DoubleHashMap(int multiplier, int modulus, int secondaryModulus) {
		hashMapSize = 4000;
		this.multiplier = multiplier;
		this.modulus = modulus;
		this.secondaryModulus = secondaryModulus;
		nodeArray = new HashMapNode[hashMapSize];
	}

	// construct a DoubleHashMap with given capacity and given hash parameters
	public DoubleHashMap(int hashMapSize, int multiplier, int modulus,
			int secondaryModulus) {
		this.hashMapSize = hashMapSize;
		this.multiplier = multiplier;
		this.modulus = modulus;
		this.secondaryModulus = secondaryModulus;
		nodeArray = new HashMapNode[this.hashMapSize];

	}

	// hashing

	// Hint: to use the output of hashing as your index into the map,
	// you will need to perform mod hashMapSize

	// The primary hash function you need to implement is:
	// hash(key) = abs(multiplier · hashCode(key)) mod modulus
	
	// and the secondary hash function you should implement is:
	// secondaryModulus − (abs(hashCode(key)) mod secondaryModulus)
	
	/*
	 * public int hash(K key) {
		// hash(key) = multiplier · abs(hashCode(key)) mod modulus
		// return Math.abs((multiplier * Math.abs(key.hashCode())) % modulus);
		int hashing = Math.abs(key.hashCode());
		int hashingTwo = multiplier * hashing % modulus;
		// int hashingTwo = ((multiplier % modulus)* (hashing % modulus)) %
		// modulus;
		return hashingTwo;
	}
	 */

	public int hash(K key) {
		return (Math.abs((multiplier * key.hashCode()) % modulus));
	}

	public int secondaryHash(K key) {
		return (secondaryModulus - (Math.abs(key.hashCode() % secondaryModulus)));
	}

	public int size() {
		return nodeCount;
	}

	public boolean isEmpty() {
		return nodeCount == 0;
	}

	// interface methods
	@SuppressWarnings("unchecked")
	public List<K> keys() {
		List<K> keyList = new ArrayList<K>();
		for(int i = 0; i < nodeArray.length; i++){
			if(nodeArray[i] != null && nodeArray[i] != defunct){
				keyList.add((K) nodeArray[i].getKey());
			}
		}		
		return keyList;

	}
	
	// Case 1 : There's a null or defunct in that index
		// Replace the contents of that entry and return null
	
	// Case 2 : There's an item in the index, and it's key is the same as the key passed in
		// Replace contents and return old value

	// Case 3 : There's an item in the index and it's key is different to the key passed in 
		// Keep trying the hashing algorithm to get a new index until you locate an empty one
	
	// Case 4 : The algorithm has wrapped around the array without finding an empty space
		// Don't put it in the array

	@SuppressWarnings("unchecked")
	public V put(K key, V value) {

		int initialHash = hash(key);
		int index = initialHash % hashMapSize;
		int initialIndex = index;
		
		boolean indexTaken = true;
		int increment = 0;
		int hash1 = hash(key);
		int hash2 = secondaryHash(key);
		
		int oldPutCollisions = putCollisions;
		

		while(indexTaken){
			int nextIndex = (hash1 + (increment * hash2)) % hashMapSize;
			
			if(increment > maxCollisions){
				maxCollisions = increment;
			}
			
			// Case 4 : Complete Wraparound 
			if(increment != 0 && nextIndex == initialIndex){
				break;
			}

			// Case 1 : Empty Slot
			if(nodeArray[nextIndex] == null || nodeArray[nextIndex] == defunct){
				HashMapNode newNode = new HashMapNode(key,value);
				nodeArray[nextIndex] = newNode;
				nodeCount++;
				indexTaken = false;
				
			}
			
			// Slot not empty
			else if(nodeArray[nextIndex] != null && nodeArray[nextIndex] != defunct){
				putCollisions = oldPutCollisions + 1;
				totalCollisions++;
				
				// Case 2 : keys match
				if(nodeArray[nextIndex].getKey() == key){
					V oldValue = (V) nodeArray[index].getValue();
					nodeArray[index].setValue(value);
					indexTaken = false;
					
					return oldValue;
				// Case 3 : keys don't match
				}else{
					increment++;
				}
			}
		}
		return null;
	}
	
	// Remove
	// Case 1 : Entry is found 
		// Return the value, and replace the Node with a defunct node
	// Case 2 : Entry is not found
		// Return null 
	@SuppressWarnings("unchecked")
	public V remove(K key) {
		boolean isFound = false;
		
		int hash1 = hash(key);
		int hash2 = secondaryHash(key);
		int increment = 0;
		int initialIndex = 0;
		
		while(!isFound){
			int index = (hash1 + (increment * hash2)) % nodeArray.length; 
			if(increment == 0){
				initialIndex = index;
			}else{
				if(index == initialIndex){
					return null;
				}
			}
			
			increment++;
			HashMapNode current = nodeArray[index];
	
			if(current != null && current != defunct){
				if(current.getKey() == key){
					V result = (V) current.getValue();
					nodeArray[index] = defunct;
					nodeCount--;
					isFound = true;
					return result;
				}
			}
			
		}
		
		return null;
	}

	@SuppressWarnings("unchecked")
	public V get(K key) {
		for (int i = 0; i < nodeArray.length; i++){
			if(nodeArray[i] != null && nodeArray[i] != defunct && nodeArray[i].getKey() == key){
				return (V) nodeArray[i].getValue();
			}
		}
		return null;
	}

	// collision statistics
	public int putCollisions() {
		return putCollisions;
	}

	public int totalCollisions() {
		return totalCollisions;
	}

	public int maxCollisions() {
		return maxCollisions;
	}

	public void resetStatistics() {
		putCollisions = 0;
		totalCollisions = 0;
		maxCollisions = 0;
	}

	public HashMapNode[] getNodeArray() {
		return nodeArray;
	}
}
