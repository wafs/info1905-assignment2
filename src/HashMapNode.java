public class HashMapNode {
	private Object key;
	private Object value;

	// construction
	public HashMapNode(Object key, Object value) {
		this.key = key;
		this.value = value;
	}

	// get methods
	public Object getKey() {
		return key;

	}

	public Object getValue() {
		return value;

	}

	// set method
	public void setValue(Object newValue) {
		value = newValue;
	}
}