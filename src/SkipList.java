import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SkipList<K extends Comparable<K>, V> {

	// Constructor Variables
	private SkipListNode head;
	private SkipListNode tail;
	private Random random;
	private static String PositiveInfinity = "+oo";
	private static String NegativeInfinity = "-oo";
	
	// Counters
	private int entries;
	private int	height;
	private int	searchSteps;
	
	
	public SkipList() {
		// Create an empty skiplist on instantiation
		SkipListNode h,t;
		h = new SkipListNode(NegativeInfinity, null);
		t = new SkipListNode(PositiveInfinity, null);
		
		h.setNext(t);
		t.setPrev(h);
		
		head = h;
		tail = t;
		
		random = new Random();
	}

	public int size() {
		return entries;
	}

	public boolean isEmpty() {
		return entries == 0;
	}

	// interface methods
	@SuppressWarnings("unchecked")
	public List<K> keys() {
		List<K> keyList = new ArrayList<K>();
		
		SkipListNode current = head;
		while (current.getBelow() != null){
			current = current.getBelow();
		}
		
		// If at head, jump to next
		if(current.getKey() == NegativeInfinity){
			current = current.getNext();
		}
		
		
		
		while(current.getKey() != PositiveInfinity){
			keyList.add((K) current.getKey());
			current = current.getNext();
		}
		
		return keyList;
	}

	
	public SkipListNode search(K key) {
		SkipListNode current = head;
		boolean canTraverse = true;

		while (canTraverse) {
			while (current.getNext().getKey() != PositiveInfinity
					&& current.getNext().getKey().compareTo((String) key) <= 0) {
				// Go right until next node is Infinity or Greater than in value
				if(current.getKey().equals(key)){
					searchSteps++;
					break;
				}
				searchSteps++;
				current = current.getNext();

			}
			// Drop to the level below
			if (current.getBelow() != null) {
				current = current.getBelow();
			} else {
				// Leave if already at the bottom
				canTraverse = false;
			}
		}
		return current;
	}

	@SuppressWarnings("unchecked")
	public V put(K key, V value) {
		// If already in the list, replace contents and
		// return old value
		SkipListNode searchedNode = search(key);

		if (key.equals(searchedNode.getKey())) {
			Object oldVal = searchedNode.getValue();
			searchedNode.setValue(value);
			return (V) oldVal;

		}

		// Otherwise, insert
		SkipListNode newNode = new SkipListNode((String) key, value);
		newNode.setPrev(searchedNode);
		newNode.setNext(searchedNode.getNext());

		searchedNode.getNext().setPrev(newNode);
		searchedNode.setNext(newNode);

		int currentLevel = 0;

		while (random.nextBoolean() == true) {
			if (currentLevel >= height) {
				// Make new level by making new head and tail node IF it's higher than current height
				// then linking them in and finally setting the head and tail
				// variable to the new nodes

				SkipListNode newHead, newTail;
				newHead = new SkipListNode(NegativeInfinity, null);
				newTail = new SkipListNode(PositiveInfinity, null);

				newHead.setNext(newTail);
				newTail.setPrev(newHead);

				newHead.setBelow(head);
				newTail.setBelow(tail);

				head.setAbove(newHead);
				tail.setAbove(newTail);

				head = newHead;
				tail = newTail;

				height++;
			}

			// Climb up the tower once it finds an entry that has another
			// level above the current one
			while (searchedNode.getAbove() == null) {
				searchedNode = searchedNode.getPrev();
			}
			searchedNode = searchedNode.getAbove();

			// Climb tower and adjust self,prev,next nodes
			SkipListNode keyNode = new SkipListNode((String) key, value);

			keyNode.setPrev(searchedNode);
			keyNode.setNext(searchedNode.getNext());
			keyNode.setBelow(newNode);

			searchedNode.getNext().setPrev(keyNode);
			searchedNode.setNext(keyNode);
			newNode.setAbove(keyNode);

			newNode = keyNode;
			currentLevel++;
		}

		entries++;

		// No entry found with given key
		return null;
	}

	@SuppressWarnings("unchecked")
	public V get(K key) {
		SkipListNode n = search(key);
		if (n == null) {
			return null;
		}

		// If the search returns the correct value
		if (key == n.getKey()) {
			return (V) n.getValue();
		} else {
			// not found
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public V remove(K key) {
		// find and remove all instances of key
		SkipListNode trash = search(key);

		if (key.compareTo((K) trash.getKey()) != 0) {
			return null;
		}

		V val = (V) trash.getValue();

		while (trash != null) {
			// Tell neighbours to point to each other instead
			trash.getPrev().setNext(trash.getNext());
			trash.getNext().setPrev(trash.getPrev());

			trash = trash.getAbove();
		}
		entries--;
		return val;
	}
	
    @SuppressWarnings("unchecked")
	public int searchSteps(String username){
    	searchSteps = 0;
    	search((K) username);
    	return searchSteps;
    }
    
    // Personal use, was curious to see what my skiplist looked like
    public void consoleSkipList(){
    	SkipListNode current = head;
    	
		while(current.getBelow() != null){
			current = current.getBelow();
		}
		int i = 0;
		while(current.getNext().getKey() != PositiveInfinity){
			SkipListNode elevator = current.getNext();
			while(elevator != null){
				System.out.print("["+i+"]" + " ==> " );
				elevator = elevator.getAbove();
			}
			i++;
			current = current.getNext();
			System.out.print("-");
			System.out.println();
		}
    }
}
