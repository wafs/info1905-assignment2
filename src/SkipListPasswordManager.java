import java.util.List;

public class SkipListPasswordManager {
	// construction
	private SkipList<String, User> skipList;
	public static final String NAME = "pwdMan";
	
	
	public SkipListPasswordManager() {
		skipList = new SkipList<String, User>();
	}
	
	// hashing
	// Using the djb2 algorithm to create hash
	public Long hash(String password) {
		if(password == null){
			return null;
		}
		
		long hash = 5381;

        for(Character c : password.toCharArray()){
            hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
        }

        return hash;
	}

	// userbase methods
	public List<String> listUsers() {
		// return an array of the usernames of the users currently stored
		return skipList.keys();
	}

	public int numberUsers() {
		return skipList.keys().size();
	}

	public void addNewUser(String username, String password) {
		
		// This app is the app name for the user
		// The password will be the password for the app
		if (!skipList.keys().contains(username)) {
			User user = new User(username);
			user.setPassword(NAME, hash(password));
			skipList.put(username, user);
		}else{
			System.out.println("User already exists.");
		}
	}

	public void deleteUser(String username, String password) {
		if (skipList.keys().contains(username)) {			
			if (skipList.get(username).getPassword(NAME).equals(hash(password))) {
				skipList.remove(username);
			} else {
				System.out.println("Failed to authenticate user.");
			}
		} else {
			System.out.println("No such user exists.");
		}
	}

	// interface methods
	public boolean authenticate(String username, String password) {
		if (skipList.keys().contains(username)) {
			if (skipList.get(username).getPassword(NAME) == (null)) {
				System.out.println("No password found.");
			} else {
				if (skipList.get(username).getPassword(NAME).equals(hash(password))) {
					return true;
				}
			}
		} else {
			System.out.println("No such user exists.");
		}

		return false;
	}

	public boolean authenticate(String username, String password, String appName) {
		if(password == null){
			System.out.println("No password found.");
			return false;
		}
		if(skipList.keys().contains(username)){
			if(skipList.get(username).getPassword(appName).equals(hash(password))){
				return true;
			}
		}else{
			System.out.println("No such user exists.");
			return false;
		}
		return false;
	}

	public void resetPassword(String username, String oldPassword,
			String newPassword) {
		if(skipList.keys().contains(username)){
			if(skipList.get(username).getPassword(NAME).equals(hash(oldPassword))){
				skipList.get(username).setPassword(NAME, hash(newPassword));
			}else{
				System.out.println("Failed to authenticate user.");
			}
		}else{
			System.out.println("No such user exists.");
		}
	}

	public void resetPassword(String username, String oldPassword,
			String newPassword, String appName) {
		if(skipList.keys().contains(username)){
			if(skipList.get(username).getPassword(appName).equals(hash(oldPassword))){
				skipList.get(username).setPassword(appName, hash(newPassword));
			}else{
				System.out.println("Failed to authenticate user.");
			}
		}else{
			System.out.println("No such user exists.");
		}
	}

	public void newAppPassword(String username, String thisPassword,
			String appPassword, String appName) {
		if(skipList.keys().contains(username)){
			if(skipList.get(username).getPassword(NAME).equals(hash(thisPassword))){
				if(!(skipList.get(username).getPassword(appName) == null)){
					System.out.println("Password already set up.");
				}else{
					skipList.get(username).setPassword(appName, hash(appPassword));
				}
			}else{
				System.out.println("Failed to authenticate user.");
			}
		}else{
			System.out.println("No such user exists");
		}
	}
	
	public int searchSteps(String username){
		return skipList.searchSteps(username);
	}
}
