public class SkipListNode {
	// construction
	private String key;
	private Object value;
	private SkipListNode prev,
						 next,
						 above,
						 below;
	
	SkipListNode(String key, Object value) {
		this.key = key;
		this.value = value;
	}

	// get methods
	public String getKey() {
		return key;
	}

	public Object getValue() {
		return value;
	}

	public SkipListNode getPrev() {
		return prev;
	}

	public SkipListNode getNext() {
		return next;
	}

	public SkipListNode getAbove() {
		return above;
	}

	public SkipListNode getBelow() {
		return below;
	}

	// set methods
	public void setValue(Object newValue) {
		value = newValue;
	}

	public void setPrev(SkipListNode prev) {
		this.prev = prev;
	}

	public void setNext(SkipListNode next) {
		this.next = next;
	}

	public void setAbove(SkipListNode above) {
		this.above = above;
	}

	public void setBelow(SkipListNode below) {
		this.below = below;
	}
}