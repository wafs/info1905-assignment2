import java.util.List;

public class PasswordManager {
	// construction
	private DoubleHashMap<String, User> dhm;
	public static final String NAME = "pwdMan";
	
	
	public PasswordManager() {
		dhm = new DoubleHashMap<String, User>(4000, 1, 4271, 647);
	}

	public PasswordManager(int size) {
		dhm = new DoubleHashMap<String, User>(size, 1, 4271, 647);
	}
	
	
	// hashing
	// Using the djb2 algorithm to create hash
	public Long hash(String password) {
		if(password == null){
			return null;
		}
		
		long hash = 5381;
        for(Character c : password.toCharArray()){
            hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
        }
        return hash;
	}

	// userbase methods
	public List<String> listUsers() {
		// return an array of the usernames of the users currently stored
		return dhm.keys();
	}

	public int numberUsers() {
		return dhm.keys().size();
	}

	public void addNewUser(String username, String password) {
		
		// This app is the app name for the user
		// The password will be the password for the app
		if (!dhm.keys().contains(username)) {
			User user = new User(username);
			user.setPassword(NAME, hash(password));
			dhm.put(username, user);
		}else{
			System.out.println("User already exists.");
		}
	}

	public void deleteUser(String username, String password) {
		if (dhm.keys().contains(username)) {			
			if (dhm.get(username).getPassword(NAME).equals(hash(password))) {
				dhm.remove(username);
			} else {
				System.out.println("Failed to authenticate user.");
			}
		} else {
			System.out.println("No such user exists.");
		}
	}

	// interface methods
	public boolean authenticate(String username, String password) {
		if (dhm.keys().contains(username)) {
			if (dhm.get(username).getPassword(NAME) == (null)) {
				System.out.println("No password found.");
			} else {
				if (dhm.get(username).getPassword(NAME).equals(hash(password))) {
					return true;
				}
			}
		} else {
			System.out.println("No such user exists.");
		}

		return false;
	}

	// Attempt to authenticate the user
	public boolean authenticate(String username, String password, String appName) {
		if(password == null){
			System.out.println("No password found.");
			return false;
		}
		if(dhm.keys().contains(username)){
			if(dhm.get(username).getPassword(appName).equals(hash(password))){
				return true;
			}
		}else{
			System.out.println("No such user exists.");
			return false;
		}
		return false;
	}

	// Password reset for pwdMan
	public void resetPassword(String username, String oldPassword,
			String newPassword) {
		if(dhm.keys().contains(username)){
			if(dhm.get(username).getPassword(NAME).equals(hash(oldPassword))){
				dhm.get(username).setPassword(NAME, hash(newPassword));
			}else{
				System.out.println("Failed to authenticate user.");
			}
		}else{
			System.out.println("No such user exists.");
		}
	}

	// Password reset for an app
	public void resetPassword(String username, String oldPassword,
			String newPassword, String appName) {
		if(dhm.keys().contains(username)){
			if(dhm.get(username).getPassword(appName) == null){
				System.out.println("No such user exists.");
			}else if(dhm.get(username).getPassword(appName).equals(hash(oldPassword))){
				dhm.get(username).setPassword(appName, hash(newPassword));
			}else{
				System.out.println("Failed to authenticate user.");
			}
		}else{
			System.out.println("No such user exists.");
		}
	}

	// add new password for app
	public void newAppPassword(String username, String thisPassword,
			String appPassword, String appName) {
		if(dhm.keys().contains(username)){
			if(dhm.get(username).getPassword(NAME).equals(hash(thisPassword))){
				if(!(dhm.get(username).getPassword(appName) == null)){
					System.out.println("Password already set up.");
				}else{
					dhm.get(username).setPassword(appName, hash(appPassword));
				}
			}else{
				System.out.println("Failed to authenticate user.");
			}
		}else{
			System.out.println("No such user exists");
		}
	}
}
