
public class User {	
	// construction
	
	private String username;
	private DoubleHashMap<String, Long> dhm;
	
	
	// construct a new User with given username and empty password store
	// store should have size 20, and use multiplier=1 modulus=23
	// secondaryModulus=11
	public User(String username) {
		this.username = username;
		dhm = new DoubleHashMap<String, Long>(20, 1, 23, 11);
	}
	
	// get methods
	public String getUsername() {
		return username;
	}

	public Long getPassword(String appName) {
		return dhm.get(appName);
	}

	// set method
	public void setPassword(String appName, Long passwordHash) {
		dhm.put(appName, passwordHash);
	}
}